import os
import math
from collada import *
from functions import *
import numpy as np
from sympy import sin, cos, pi
import cv2
import sys
import random

# ipath="C:/Users/Admin/Desktop/imagesheet.png"
# spath= "E:/semester_5/Research_Project/unity_adl_julian"
spath= sys.argv[1]
homography=[]
col_files = []                                  #saving all xml file
jointscoordinates=[]                            #Creating empty list/array
nodeid = []                                     #creating empty list/array
RectifiedCoordinatescam2=[]
RectifiedCoordinatescam1=[]
#TwoDImagepoints = []

Disparity=[]
Depth=[]
origin = np.array([[0], [0], [0], [1]])
images = []
flip_at_origin = True
baseline = 0.3

width = 1024
f = width / math.pi
c_xy = (width - 1) / 2
K_matrix = np.array([[f, 0, c_xy], [0, f, c_xy], [0, 0, 1]])
dist_coeffs = np.array([0., 0., 0., 0.]) # the  Co-efficient k which leads upto atleast 4 values as shown


def calc_coords(node):
    matrix = node.matrix
    own_coords = node.matrix.dot(origin)

    return_ids = [node.id]
    return_coords = [own_coords]

    for child in node.children:
        child_result_ids, child_result_coords = calc_coords(child)
        child_results_coords_corrected = []
        for coords in child_result_coords:
            child_results_coords_corrected += [matrix.dot(coords)]

        return_ids += child_result_ids
        return_coords += child_results_coords_corrected

    return return_ids, return_coords

def parsingAnd3Dcoordinates():
    for roots,dirs,files in os.walk(spath):
        for file in files:
            if file=="0000220_bones.dae":
                col_files.append(file)


    random.shuffle(col_files)

    for x in col_files:
        SelectedFile = "".join((spath,"/", x))
        print(SelectedFile)
        mesh = Collada(SelectedFile)
        root_node = mesh.scene.nodes[0]
        all_node_ids, all_node_coords = calc_coords(root_node)
        nodeid.append(all_node_ids)
        images.append(SelectedFile.replace("bones.dae", "img.png"))

        for idx, eachjoint in enumerate(all_node_coords):
            p=all_node_coords[idx]
            temp = p[2][0]                                 #swaping y places of y and z axiss
            p[2][0] = p[1][0]                               #swaping y places of y and z axis
            p[1][0] = temp                                   #swaping y places of y and z axis
            p = np.delete(p, 3, axis=0)                       # deleting homogeneous coordinate so that it will not be scaled
            scale = 1.6 / 3                                    # 3 unity units are 1.6 m
            p=np.dot(p, scale)
            r = np.array([[1]])
            p = np.append(p, r, axis=0)
            jointscoordinates.append(p)


        #break (for checking purpose only)

#.............................................................................................................................................................
parsingAnd3Dcoordinates()                    #calling function

def Coordinatesconversion(baseline,center):
    row1=[1,0,0]                               #first row of rotation matrix
    row2=[0,cos(pi),-sin(pi)]                  #second row of rotation matrix
    row3=[0,sin(pi),cos(pi)]                   #third row of rotation matrix
    r=np.matrix([row1,row2,row3])             #Rotation around x-axis
    c=np.matrix([baseline,0,center]).transpose()     #transpose of camera center in WCS
    result=np.dot(-r,c)                       #Translation (-RC)
    Homo = np.hstack((r, result))
    Homo = np.vstack((Homo, np.matrix([[0, 0, 0, 1]])))
    print(Homo)
    exit(0)
    Ccam=np.array([[0],[0],[0]])
    Z_axis=np.matrix([0,0,1]).transpose()              #Z axis of vector
    focal_length=1                             # I define focal length =1 but i will define the real focal length while initializing the K matrix
    #------------------------------------------------------------------------------------------------------------------
    #K-MATRIX: (GLOBAL VARIABLES)
    TwoDImagepoints = []
    for idx,eachjoint in enumerate(jointscoordinates):    # idx is the loop iteration
        Xcam=np.dot(Homo,eachjoint)                       #conversion of Xworld to Xcam
        #...................................................................................................................................
        #Calculating Xnorm:
        #(1):Finding ROW(P(theeta):
        Xcam = np.delete(Xcam, 3, axis = 0)     # Xcam non homogeneous
        Vector_b=np.subtract(Xcam,Ccam)
        Vector_a=np.subtract(Z_axis,Ccam)
        Vector_a=np.matrix(Vector_a).transpose()
        #Angle of elevation:
        Numerator=np.dot(Vector_a,Vector_b)
        Vector_b=np.array(Vector_b)
        Vector_b=np.array(Vector_b,dtype=np.float32)                         #Conversion into Floating points so that program will take large floating point values
        Vector_a=np.array(Vector_a,dtype=np.float32)                         #Conversion into Floating points so that program will take large floating point values
        Modulus_a=np.linalg.norm(Vector_a)
        Modulus_b=np.linalg.norm(Vector_b)
        Denomenator=Modulus_a*Modulus_b
        rho=math.acos(Numerator/Denomenator)                             #hence p(theeta) or rho(theeta) had been calculated
        rho=rho*focal_length                                                     # omni directional camera follows equidistance geometry hence multiplying it by focal length (2048)
        #................................................................................................................
        #Calculating unit Vector u(φ):
        Xcam = np.delete(Xcam, 2, axis = 0)
        x_axis=np.matrix([1,0])
        Neumerator=np.dot(x_axis,Xcam)
        Neumerator=Neumerator[0,0]
        Xcam=np.array(Vector_b,dtype=np.float32)                         #Conversion into Floating points so that program will take large floating point values
        x_axis=np.array(Vector_a,dtype=np.float32)                         #Conversion into Floating points so that program will take large floating point values
        Xcam = np.delete(Xcam, 2, axis = 0)
        Modulus_Xcam=np.linalg.norm(Xcam)
        Modulus_x_axis=np.linalg.norm(x_axis)
        Denomienator=Modulus_Xcam*Modulus_x_axis
        cos_phi=Neumerator/Denomienator
        sin_phi=math.sqrt(1-cos_phi*cos_phi)
        u=np.array([cos_phi,sin_phi])
        Xnorm=rho*u
        if idx == 0:
            print()
            print("Xnorm")
            print(Xnorm)
            print()
        Xnorm=np.insert(Xnorm, 2, 1)
        Xnorm=np.array([Xnorm]).transpose()
        if flip_at_origin:
            Xnorm[0] = Xnorm[0] * -1
            Xnorm[1] = Xnorm[1] * -1
        Ximg=np.dot(K_matrix,Xnorm)
        if idx == 0:
            print()
            print("Ximg")
            print(Ximg)
            print()
        #print("2D joint coordinates: ",idx)
        Ximg = np.delete(Ximg, 2, axis=0)
        TwoDImagepoints.append(Ximg)
    print(";;;;;;;;;;;;;;;;;;;;;;;;;;;")
    print(TwoDImagepoints)
    exit(0)
    #print(len(TwoDImagepoints))
    tensor = np.concatenate(TwoDImagepoints,axis=1)
    return np.array(tensor, dtype=np.float32)


#****************************************************************************************************************************************************************************
#Rectification:
tensor = Coordinatesconversion(0,2.5)

Ximg_tensor = np.expand_dims(tensor.transpose(), axis=0)
Ximg_undist_tensor = cv2.fisheye.undistortPoints(Ximg_tensor, K=K_matrix, D=dist_coeffs, P=K_matrix)
print(Ximg_undist_tensor)
tensor = Coordinatesconversion(baseline ,2.5)

Ximg_tensor2 = np.expand_dims(tensor.transpose(), axis=0)
Ximg_undist_tensor2 = cv2.fisheye.undistortPoints(Ximg_tensor2, K=K_matrix, D=dist_coeffs, P=K_matrix)
#........................................................................................................................
#Disparity:
Disparity =  Ximg_undist_tensor2[0, :, 0] - Ximg_undist_tensor[0, :, 0]

print("Disparity final")
print(Disparity)
print(type(Disparity))
print(np.shape(Disparity))
#........................................................................................................................
#Depth:
Neu=f*baseline
Depth=Neu/Disparity

print(Depth)
#.......................................................................................................................
#Drawing
canvas = cv2.imread(images[0])
print(canvas.shape)
canvas = cv2.resize(canvas,(width, width))
print(canvas.shape)

persp_canvas = cv2.fisheye.undistortImage(canvas, K_matrix, D=dist_coeffs, Knew=K_matrix)


#print(canvas.shape)
canvas = draw2D(Ximg_undist_tensor, width, width, persp_canvas, images[0], (255, 0, 0))
cv2.imshow("skeletons", canvas)
cv2.waitKey(-1)

canvas = draw2D(Ximg_undist_tensor2, width, width, persp_canvas, images[0], (0, 0, 255))
cv2.imshow("skeletons", canvas)
cv2.waitKey(-1)
#.......................................................................................................................
#Getting Xworld back..
